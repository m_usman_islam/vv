import { reactive } from 'vue';
import { required, email, helpers, alpha } from '@vuelidate/validators';
import useVuelidate from '@vuelidate/core';

//genral defication of rules
const generalRules = {
  //rule for first name and last name
  firstNameLastName: {
    required: helpers.withMessage(
      ({ $model }) => ` First Name  cant be empty`,
      required
    ),
    alpha: helpers.withMessage(
      'First Name cant be numaric or special chracteros',
      alpha
    ),
    $autoDirty: true,
  },
  //rule for email
  email: { required, $autoDirty: true, email },
};

//User Info state and validation
export const userState = reactive({
  firstName: '',
  lastName: '',
  email: '',
});
export const userRules = {
  firstName: {
    ...generalRules.firstNameLastName,
  }, // Matches state.firstName
  lastName: {
    ...generalRules.firstNameLastName,
  }, // Matches state.lastName
  email: { ...generalRules.email },
};

export const userRulesVidation = useVuelidate(userRules, userState);
